##print_string.c

print_string.c is *C* program that creates a string a limited character defined by a constant **MAXSIZE**.

####How to run the print_string.c?
run **make print_string** inside the directory "strings"

##Packetize.h pckTest.c

packetize.h combines a set of structures and functions divides the array of string into small structures of size **num_packets** (maximum size is PACKET_SIZE) and contains a character array *packets*. 
 
The pckTest takes a document, reads it, and tests the **packetize.c** by dividing the document into small packets and ask the user to pick which one of the packets they want to see in the terminal. 

####how to run it?

in order to run the pckTest.c, the user should run the command **make** in the command line inside the directory "packetize".
