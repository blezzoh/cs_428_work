#include <stdio.h> 
#include <stdlib.h>

#define MAXSIZE 100


int getaline(char c[], int i);
void space_free_print(char * c, int size);

int main(){
	
	char *c;
	int len;

	c =(char *) malloc(sizeof(char) * MAXSIZE);

	if(c == NULL){
		printf("failed");
		return 1;
	}

    len = getaline(c, MAXSIZE);

    printf("\n");

    for (int i =0; i<MAXSIZE && c[i] != 0 & len != -1; i++){
    	printf("%c", c[i] );
    }

    printf("\n------ printing without space ----------\n \n");

    space_free_print(c, len);
	free(c);
	return 0;

 }


int getaline(char s[],int lim) {
   int c, i;
   for (i=0; i < lim && (c=getchar())!=EOF && c!='\n'; ++i){

   	/*instead of stopping the for loop on lim-1, i did lim so that i can catch the array  out
   	bound error*/

   	  if(i == lim-1){
   	  	printf("\nRIDICULOUS ERROR: array out of bound");
   	  	return -1;
   	  }
      s[i] = c;
   }
   if (c == '\n') {
      s[i] = c;
      ++i;
   }
   s[i] = '\0';
   return i;
}

// function to print without a free space 

void space_free_print(char * c, int size){

	if(size != -1){
		for(int i=0; i < size; i++){
			if((int) c[i] != 32 ){
				printf("%c", c[i]);
			}
		}
	}
} 
